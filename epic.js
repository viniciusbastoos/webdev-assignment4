(function() {
    var imageListData = [];

    document.addEventListener("DOMContentLoaded", function() {
        console.log("DOM content loaded");

        var searchButton = document.getElementById("searchButton");
        searchButton.addEventListener("click", function() {
            handleFormSubmit();
        });

        var imageMenu = document.getElementById("image-menu");
        imageMenu.addEventListener("click", function(event) {
            var target = event.target;
            if (target.tagName === "SPAN" && target.parentNode.tagName === "LI") {
                var imageIndex = target.parentNode.getAttribute("data-image-list-index");
                displaySelectedImage(imageIndex);
            }
        });
    });

    function handleFormSubmit() {
        var type = document.getElementById("type").value;
        var date = document.getElementById("date").value;
    
        var apiUrl;
        if (date) {
            apiUrl = "https://epic.gsfc.nasa.gov/api/" + type + "/date/" + date;
        } else {
            apiUrl = "https://epic.gsfc.nasa.gov/api/" + type + "/all";
        }
    
        fetch(apiUrl)
            .then(function(response) {
                return response.json();
            })
            .then(function(data) {
                console.log("Data fetched:", data);
                imageListData = data;
    
                var indexOfEnhanced = getIndexByImageType("enhanced");
                console.log("Index of 'enhanced' image type:", indexOfEnhanced);
    
                displayImages(data);
            })
            .catch(function(error) {
                console.error("Error fetching data:", error);
            });
    }

    function getIndexByImageType(imageType) {
        for (var i = 0; i < imageListData.length; i++) {
            if (imageListData[i].image.toLowerCase() === imageType.toLowerCase()) {
                return i;
            }
        }
        return -1; // Not found
    }

    function displayImages(data) {
        var imageMenu = document.getElementById("image-menu");
        imageMenu.innerHTML = "";

        data.forEach(function(image, index) {
            var listItem = document.getElementById("image-menu-item").content.cloneNode(true);
            listItem.querySelector("span.image-list-title").innerText = "Image " + (index + 1);
            listItem.querySelector("li").setAttribute("data-image-list-index", index);
            imageMenu.appendChild(listItem);
        });
    }

    /* IM HAVING TROUBLE HERE... :( */
    function displaySelectedImage(index) {
        var selectedImage = document.getElementById("earth-image");
        var imageUrl = "epic_1b";
        selectedImage.src = imageUrl;

        var imageDate = document.getElementById("earth-image-date");
        var imageTitle = document.getElementById("earth-image-title");

        // Set the date and title depending on the image
        var date = new Date(imageListData[index].date);
        imageDate.innerText = "Date: " + date.toDateString();
        imageTitle.innerText = "Title: " + imageListData[index].caption;
    }
})();
